package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    //    Create course
    public void createCourse(String stringToken, Course course){

        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //    Get All Courses
    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    //    Delete course
    public ResponseEntity deleteCourse(Long id, String stringToken){


        Course courseForDeleting = courseRepository.findById(id).get();
        String CourseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(CourseAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }

    }

    //    Update course
    public ResponseEntity updateCourse(Long id, String stringToken, Course course){


        Course courseForUpdating = courseRepository.findById(id).get();
//        Get the "author" of the specific course.
        String courseAuthorName = courseForUpdating.getUser().getUsername();
//          Get the "username" from the stringToken to compare it with the username of the current course being edited.
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        // check if the username of the authenticated user matches the username of the post's author
        if(authenticatedUserName.equals(courseAuthorName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());

            courseRepository.save(courseForUpdating);

            return  new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Get user's posts
    public Iterable<Course> getMyCourses(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }
}
