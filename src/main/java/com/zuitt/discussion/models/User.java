package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
//Designate the table name related to the model.
@Table(name = "users")
public class User {
    @Id
//    id will be auto-incremented.
    @GeneratedValue
    private Long id;

    @Column
    private String username;
    @Column
    private String password;

    @OneToMany(mappedBy = "user")

    @JsonIgnore

    private Set<Course> courses;

    public User() {}
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public  Long getId(){
        return id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public  Set<Course> getCourses(){
        return courses;
    }

    public void setCourses(Set<Course> courses){
        this.courses = courses;
    }

}
